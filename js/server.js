const WebSocket = require('ws')
require('./data.js')

const wss = new WebSocket.Server({
  port: 8080
})

wss.on('connection', ws => {
  console.log("Connected!")
  console.log(wss.address())

  let interval

  ws.on('message', msg => {
    console.log(`received: ${msg}`)
    if (msg.startsWith('subscribe')) {
      const id = msg.split('/')[1]
      console.log(`Subscribed to feed ${id}`)
      startSendingDataFeed(id)
    }
  })

  const startSendingDataFeed = (id) => {
    clearInterval(interval)
    interval = setInterval(() => {
      const randomIndex = getRandomInt(data.length)
      const dataStr = JSON.stringify(data[randomIndex])
      console.log(`Sending ${dataStr}`)
      ws.send(dataStr)
    }, 3000)
  }
})

const getRandomInt = (max) => {
  return Math.floor(Math.random() * max)
}
