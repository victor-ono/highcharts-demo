import Component from '../lib/component.js'
import store from '../store/index.js'

export default class RadarChart extends Component {
  constructor(ws) {
    super({
      store
    })
    this.ws = ws
    this.ws.addEventListener('message', (event) => {
      const data = JSON.parse(event.data)
      console.log('Received in RadarChart:', data)
      this.chart.series[0].setData(this.convertToSeriesData(data), true)
    })
  }

  getSelectedItemData = () => {
    return store.state.data[store.state.selectedItem]
  }

  render() {
    console.log('Render RadarChart')
    const data = this.getSelectedItemData()
    this.chart = Highcharts.chart('radar-chart', {
      chart: {
        polar: true
      },
      title: {
        text: 'Dispersion Radar Chart'
      },
      pane: {
        startAngle: 0,
        endAngle: 360
      },
      xAxis: {
        tickInterval: 45,
        min: 0,
        max: 360,
        labels: {
          format: '{value}'
        }
      },
      yAxis: {
        min: 0
      },
      plotOptions: {
        series: {
          pointStart: 0,
          pointInterval: 45
        },
        column: {
          pointPadding: 0,
          groupPadding: 0
        }
      },
      series: [{
        type: 'area',
        name: 'Dispersion',
        data: [
          data['DISPERSION_RANGE1'],
          data['DISPERSION_RANGE2'],
          data['DISPERSION_RANGE3'],
          data['DISPERSION_RANGE4'],
          data['DISPERSION_RANGE5']
        ]
      }]
    })
  }
}
