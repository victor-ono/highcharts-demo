import Component from '../lib/component.js'
import store from '../store/index.js'

export default class ColChart extends Component {
  constructor(ws) {
    super({
      store
    })
    this.ws = ws
    this.ws.addEventListener('message', (event) => {
      const data = JSON.parse(event.data)
      console.log('Received in ColChart:', data)
      for (let i = 0; i < 5; i++) {
        this.chart.series[i].setData([data['DISPERSION_RANGE' + i]], true)
      }
    })
  }

  getSelectedItemData = () => {
    return store.state.data[store.state.selectedItem]
  }

  render = () => {
    const data = this.getSelectedItemData()
    this.chart = Highcharts.chart('col-chart', {
      chart: {
        type: 'column'
      },
      title: {
        text: 'Dispersion'
      },
      xAxis: {
        categories: ['Score']
      },
      yAxis: {
        title: {
          text: 'Relative Frequency'
        }
      },
      plotOptions: {
        column: {
          pointPadding: 0,
          borderWidth: 0
        }
      },
      series: [
        {
          name: '0-20',
          data: [data['DISPERSION_RANGE1']]
        },
        {
          name: '20-40',
          data: [data['DISPERSION_RANGE2']]
        },
        {
          name: '40-60',
          data: [data['DISPERSION_RANGE3']]
        },
        {
          name: '60-80',
          data: [data['DISPERSION_RANGE4']]
        },
        {
          name: '80-100',
          data: [data['DISPERSION_RANGE5']]
        }
      ]
    })
  }
}
