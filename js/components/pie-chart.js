import Component from '../lib/component.js'
import store from '../store/index.js'

export default class PieChart extends Component {
  constructor(ws) {
    super({
      store
    })
    this.ws = ws
    this.ws.addEventListener('message', (event) => {
      const data = JSON.parse(event.data)
      console.log('Received in PieChart:', data)
      this.chart.series[0].setData(this.convertToSeriesData(data), true)
    })
  }

  getSelectedItemData = () => {
    return store.state.data[store.state.selectedItem]
  }

  convertToSeriesData = (data) => {
    return [
      {
        name: `Covered (${data['COMPLETENESS_COUNT']})`,
        y: data['COMPLETENESS_COUNT']
      },
      {
        name:  `Not Covered (${data['AVAILABLE_TOTAL_COUNT'] - data['COMPLETENESS_COUNT']})`,
        y: data['AVAILABLE_TOTAL_COUNT'] - data['COMPLETENESS_COUNT']
      }
    ]
  }

  render() {
    console.log('Render PieChart')

    this.chart = Highcharts.chart('pie-chart', {
      chart: {
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false,
        type: 'pie'
      },
      title: {
        text: 'Completeness'
      },
      tooltip: {
        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
      },
      accessibility: {
        point: {
          valueSuffix: '%'
        }
      },
      plotOptions: {
        pie: {
          allowPointSelect: true,
          cursor: 'pointer',
          dataLabels: {
            enabled: true,
            format: '<b>{point.name}</b>: {point.percentage:.1f} %'
          },
          size: 250
        }
      },
      series: [{
        name: 'Completeness',
        colorByPoint: true,
        data: this.convertToSeriesData(this.getSelectedItemData())
      }]
    })
  }
}
