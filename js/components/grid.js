import Component from '../lib/component.js'
import store from '../store/index.js'

export default class Grid extends Component {
  constructor(ws) {
    super({
      store
    })
    const gridDiv = document.querySelector('#myGrid')
    new agGrid.Grid(gridDiv, {
      columnDefs: [
        { headerName: 'Vendor', field: 'Vendor' },
        { headerName: 'Taxonomy ID', field: 'Taxonomy ID' },
        { headerName: 'Total count', field: 'AVAILABLE_TOTAL_COUNT' },
        { headerName: 'Completeness', field: 'COMPLETENESS_COUNT' },
        { headerName: 'Dispersion 1', field: 'DISPERSION_RANGE1' },
        { headerName: 'Dispersion 2', field: 'DISPERSION_RANGE2' },
        { headerName: 'Dispersion 3', field: 'DISPERSION_RANGE3' },
        { headerName: 'Dispersion 4', field: 'DISPERSION_RANGE4' },
        { headerName: 'Dispersion 5', field: 'DISPERSION_RANGE5' },
      ],
      defaultColDef: {
        width: 110
      },
      rowData: store.state.data,
      onRowClicked: (event) => {
        ws.send(`subscribe/${event.rowIndex}`)
        store.dispatch('setSelectedItem', event.rowIndex)
      }
    })
  }

  render() {
    console.log("Render Grid")
  }
}
