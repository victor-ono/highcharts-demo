data = [
  {
    "EFFECTIVE_DATE": "5/7/20",
    "Vendor": "v0001",
    "Taxonomy ID": 36,
    "AVAILABLE_TOTAL_COUNT": 6859,
    "COMPLETENESS_COUNT": 777,
    "DISPERSION_RANGE1": 108,
    "DISPERSION_RANGE2": 251,
    "DISPERSION_RANGE3": 245,
    "DISPERSION_RANGE4": 125,
    "DISPERSION_RANGE5": 48
  },
  {
    "EFFECTIVE_DATE": "5/7/20",
    "Vendor": "v0001",
    "Taxonomy ID": 37,
    "AVAILABLE_TOTAL_COUNT": 6859,
    "COMPLETENESS_COUNT": 637,
    "DISPERSION_RANGE1": 41,
    "DISPERSION_RANGE2": 183,
    "DISPERSION_RANGE3": 249,
    "DISPERSION_RANGE4": 112,
    "DISPERSION_RANGE5": 52
  },
  {
    "EFFECTIVE_DATE": "5/7/20",
    "Vendor": "v0001",
    "Taxonomy ID": 35,
    "AVAILABLE_TOTAL_COUNT": 6859,
    "COMPLETENESS_COUNT": 2953,
    "DISPERSION_RANGE1": 374,
    "DISPERSION_RANGE2": 605,
    "DISPERSION_RANGE3": 954,
    "DISPERSION_RANGE4": 544,
    "DISPERSION_RANGE5": 476
  },
  {
    "EFFECTIVE_DATE": "5/7/20",
    "Vendor": "v0001",
    "Taxonomy ID": 24,
    "AVAILABLE_TOTAL_COUNT": 20577,
    "COMPLETENESS_COUNT": 2504,
    "DISPERSION_RANGE1": 142,
    "DISPERSION_RANGE2": 1050,
    "DISPERSION_RANGE3": 969,
    "DISPERSION_RANGE4": 291,
    "DISPERSION_RANGE5": 52
  },
  {
    "EFFECTIVE_DATE": "5/7/20",
    "Vendor": "v0001",
    "Taxonomy ID": 41,
    "AVAILABLE_TOTAL_COUNT": 6859,
    "COMPLETENESS_COUNT": 1687,
    "DISPERSION_RANGE1": 126,
    "DISPERSION_RANGE2": 910,
    "DISPERSION_RANGE3": 503,
    "DISPERSION_RANGE4": 119,
    "DISPERSION_RANGE5": 29
  },
  {
    "EFFECTIVE_DATE": "5/7/20",
    "Vendor": "v0001",
    "Taxonomy ID": 42,
    "AVAILABLE_TOTAL_COUNT": 6859,
    "COMPLETENESS_COUNT": 633,
    "DISPERSION_RANGE1": 4,
    "DISPERSION_RANGE2": 114,
    "DISPERSION_RANGE3": 393,
    "DISPERSION_RANGE4": 111,
    "DISPERSION_RANGE5": 11
  },
  {
    "EFFECTIVE_DATE": "5/7/20",
    "Vendor": "v0001",
    "Taxonomy ID": 43,
    "AVAILABLE_TOTAL_COUNT": 6859,
    "COMPLETENESS_COUNT": 206,
    "DISPERSION_RANGE1": 12,
    "DISPERSION_RANGE2": 45,
    "DISPERSION_RANGE3": 76,
    "DISPERSION_RANGE4": 61,
    "DISPERSION_RANGE5": 12
  },
  {
    "EFFECTIVE_DATE": "5/7/20",
    "Vendor": "v0001",
    "Taxonomy ID": 31,
    "AVAILABLE_TOTAL_COUNT": 6859,
    "COMPLETENESS_COUNT": 6801,
    "DISPERSION_RANGE1": 177,
    "DISPERSION_RANGE2": 367,
    "DISPERSION_RANGE3": 1196,
    "DISPERSION_RANGE4": 2861,
    "DISPERSION_RANGE5": 2200
  },
  {
    "EFFECTIVE_DATE": "5/7/20",
    "Vendor": "v0001",
    "Taxonomy ID": 32,
    "AVAILABLE_TOTAL_COUNT": 6859,
    "COMPLETENESS_COUNT": 674,
    "DISPERSION_RANGE1": 178,
    "DISPERSION_RANGE2": 214,
    "DISPERSION_RANGE3": 136,
    "DISPERSION_RANGE4": 74,
    "DISPERSION_RANGE5": 72
  },
  {
    "EFFECTIVE_DATE": "5/7/20",
    "Vendor": "v0001",
    "Taxonomy ID": 34,
    "AVAILABLE_TOTAL_COUNT": 6859,
    "COMPLETENESS_COUNT": 195,
    "DISPERSION_RANGE1": 22,
    "DISPERSION_RANGE2": 62,
    "DISPERSION_RANGE3": 64,
    "DISPERSION_RANGE4": 33,
    "DISPERSION_RANGE5": 14
  },
  {
    "EFFECTIVE_DATE": "5/7/20",
    "Vendor": "v0001",
    "Taxonomy ID": 38,
    "AVAILABLE_TOTAL_COUNT": 6859,
    "COMPLETENESS_COUNT": 1802,
    "DISPERSION_RANGE1": 176,
    "DISPERSION_RANGE2": 562,
    "DISPERSION_RANGE3": 661,
    "DISPERSION_RANGE4": 355,
    "DISPERSION_RANGE5": 48
  },
  {
    "EFFECTIVE_DATE": "5/7/20",
    "Vendor": "v0001",
    "Taxonomy ID": 33,
    "AVAILABLE_TOTAL_COUNT": 6859,
    "COMPLETENESS_COUNT": 639,
    "DISPERSION_RANGE1": 212,
    "DISPERSION_RANGE2": 189,
    "DISPERSION_RANGE3": 155,
    "DISPERSION_RANGE4": 63,
    "DISPERSION_RANGE5": 20
  },
  {
    "EFFECTIVE_DATE": "5/7/20",
    "Vendor": "v0001",
    "Taxonomy ID": 40,
    "AVAILABLE_TOTAL_COUNT": 6859,
    "COMPLETENESS_COUNT": 70,
    "DISPERSION_RANGE1": 8,
    "DISPERSION_RANGE2": 18,
    "DISPERSION_RANGE3": 23,
    "DISPERSION_RANGE4": 16,
    "DISPERSION_RANGE5": 5
  },
  {
    "EFFECTIVE_DATE": "5/7/20",
    "Vendor": "v0001",
    "Taxonomy ID": 39,
    "AVAILABLE_TOTAL_COUNT": 6859,
    "COMPLETENESS_COUNT": 232,
    "DISPERSION_RANGE1": 40,
    "DISPERSION_RANGE2": 59,
    "DISPERSION_RANGE3": 70,
    "DISPERSION_RANGE4": 42,
    "DISPERSION_RANGE5": 21
  },
  {
    "EFFECTIVE_DATE": "5/7/20",
    "Vendor": "v0001",
    "Taxonomy ID": 66,
    "AVAILABLE_TOTAL_COUNT": 6859,
    "COMPLETENESS_COUNT": 636,
    "DISPERSION_RANGE1": 27,
    "DISPERSION_RANGE2": 62,
    "DISPERSION_RANGE3": 210,
    "DISPERSION_RANGE4": 248,
    "DISPERSION_RANGE5": 89
  },
  {
    "EFFECTIVE_DATE": "5/7/20",
    "Vendor": "v0001",
    "Taxonomy ID": 62,
    "AVAILABLE_TOTAL_COUNT": 6859,
    "COMPLETENESS_COUNT": 6720,
    "DISPERSION_RANGE1": 139,
    "DISPERSION_RANGE2": 428,
    "DISPERSION_RANGE3": 1235,
    "DISPERSION_RANGE4": 2561,
    "DISPERSION_RANGE5": 2357
  },
  {
    "EFFECTIVE_DATE": "5/7/20",
    "Vendor": "v0001",
    "Taxonomy ID": 60,
    "AVAILABLE_TOTAL_COUNT": 6859,
    "COMPLETENESS_COUNT": 6739,
    "DISPERSION_RANGE1": 1453,
    "DISPERSION_RANGE2": 607,
    "DISPERSION_RANGE3": 1647,
    "DISPERSION_RANGE4": 2436,
    "DISPERSION_RANGE5": 596
  },
  {
    "EFFECTIVE_DATE": "5/7/20",
    "Vendor": "v0001",
    "Taxonomy ID": 59,
    "AVAILABLE_TOTAL_COUNT": 6859,
    "COMPLETENESS_COUNT": 6762,
    "DISPERSION_RANGE1": 93,
    "DISPERSION_RANGE2": 458,
    "DISPERSION_RANGE3": 1420,
    "DISPERSION_RANGE4": 2570,
    "DISPERSION_RANGE5": 2221
  },
  {
    "EFFECTIVE_DATE": "5/7/20",
    "Vendor": "v0001",
    "Taxonomy ID": 65,
    "AVAILABLE_TOTAL_COUNT": 6859,
    "COMPLETENESS_COUNT": 3830,
    "DISPERSION_RANGE1": 545,
    "DISPERSION_RANGE2": 1232,
    "DISPERSION_RANGE3": 1311,
    "DISPERSION_RANGE4": 591,
    "DISPERSION_RANGE5": 151
  },
  {
    "EFFECTIVE_DATE": "5/7/20",
    "Vendor": "v0001",
    "Taxonomy ID": 64,
    "AVAILABLE_TOTAL_COUNT": 6859,
    "COMPLETENESS_COUNT": 2439,
    "DISPERSION_RANGE1": 0,
    "DISPERSION_RANGE2": 72,
    "DISPERSION_RANGE3": 2367,
    "DISPERSION_RANGE4": 0,
    "DISPERSION_RANGE5": 0
  },
  {
    "EFFECTIVE_DATE": "5/7/20",
    "Vendor": "v0001",
    "Taxonomy ID": 61,
    "AVAILABLE_TOTAL_COUNT": 6859,
    "COMPLETENESS_COUNT": 6714,
    "DISPERSION_RANGE1": 145,
    "DISPERSION_RANGE2": 467,
    "DISPERSION_RANGE3": 1963,
    "DISPERSION_RANGE4": 1633,
    "DISPERSION_RANGE5": 2506
  },
  {
    "EFFECTIVE_DATE": "5/7/20",
    "Vendor": "v0001",
    "Taxonomy ID": 63,
    "AVAILABLE_TOTAL_COUNT": 6859,
    "COMPLETENESS_COUNT": 2443,
    "DISPERSION_RANGE1": 0,
    "DISPERSION_RANGE2": 153,
    "DISPERSION_RANGE3": 2290,
    "DISPERSION_RANGE4": 0,
    "DISPERSION_RANGE5": 0
  },
  {
    "EFFECTIVE_DATE": "5/7/20",
    "Vendor": "v0001",
    "Taxonomy ID": 44,
    "AVAILABLE_TOTAL_COUNT": 6859,
    "COMPLETENESS_COUNT": 3843,
    "DISPERSION_RANGE1": 162,
    "DISPERSION_RANGE2": 690,
    "DISPERSION_RANGE3": 1416,
    "DISPERSION_RANGE4": 1016,
    "DISPERSION_RANGE5": 559
  },
  {
    "EFFECTIVE_DATE": "5/7/20",
    "Vendor": "v0001",
    "Taxonomy ID": 46,
    "AVAILABLE_TOTAL_COUNT": 6859,
    "COMPLETENESS_COUNT": 6287,
    "DISPERSION_RANGE1": 1098,
    "DISPERSION_RANGE2": 2147,
    "DISPERSION_RANGE3": 1774,
    "DISPERSION_RANGE4": 846,
    "DISPERSION_RANGE5": 422
  },
  {
    "EFFECTIVE_DATE": "5/7/20",
    "Vendor": "v0001",
    "Taxonomy ID": 47,
    "AVAILABLE_TOTAL_COUNT": 6859,
    "COMPLETENESS_COUNT": 355,
    "DISPERSION_RANGE1": 12,
    "DISPERSION_RANGE2": 58,
    "DISPERSION_RANGE3": 131,
    "DISPERSION_RANGE4": 107,
    "DISPERSION_RANGE5": 47
  },
  {
    "EFFECTIVE_DATE": "5/7/20",
    "Vendor": "v0001",
    "Taxonomy ID": 54,
    "AVAILABLE_TOTAL_COUNT": 6859,
    "COMPLETENESS_COUNT": 351,
    "DISPERSION_RANGE1": 16,
    "DISPERSION_RANGE2": 102,
    "DISPERSION_RANGE3": 97,
    "DISPERSION_RANGE4": 95,
    "DISPERSION_RANGE5": 41
  },
  {
    "EFFECTIVE_DATE": "5/7/20",
    "Vendor": "v0001",
    "Taxonomy ID": 51,
    "AVAILABLE_TOTAL_COUNT": 6859,
    "COMPLETENESS_COUNT": 3764,
    "DISPERSION_RANGE1": 370,
    "DISPERSION_RANGE2": 766,
    "DISPERSION_RANGE3": 1260,
    "DISPERSION_RANGE4": 777,
    "DISPERSION_RANGE5": 591
  },
  {
    "EFFECTIVE_DATE": "5/7/20",
    "Vendor": "v0001",
    "Taxonomy ID": 45,
    "AVAILABLE_TOTAL_COUNT": 6859,
    "COMPLETENESS_COUNT": 1555,
    "DISPERSION_RANGE1": 152,
    "DISPERSION_RANGE2": 359,
    "DISPERSION_RANGE3": 518,
    "DISPERSION_RANGE4": 407,
    "DISPERSION_RANGE5": 119
  },
  {
    "EFFECTIVE_DATE": "5/7/20",
    "Vendor": "v0001",
    "Taxonomy ID": 52,
    "AVAILABLE_TOTAL_COUNT": 6859,
    "COMPLETENESS_COUNT": 438,
    "DISPERSION_RANGE1": 75,
    "DISPERSION_RANGE2": 180,
    "DISPERSION_RANGE3": 78,
    "DISPERSION_RANGE4": 61,
    "DISPERSION_RANGE5": 44
  },
  {
    "EFFECTIVE_DATE": "5/7/20",
    "Vendor": "v0001",
    "Taxonomy ID": 55,
    "AVAILABLE_TOTAL_COUNT": 6859,
    "COMPLETENESS_COUNT": 125,
    "DISPERSION_RANGE1": 12,
    "DISPERSION_RANGE2": 33,
    "DISPERSION_RANGE3": 26,
    "DISPERSION_RANGE4": 35,
    "DISPERSION_RANGE5": 19
  },
  {
    "EFFECTIVE_DATE": "5/7/20",
    "Vendor": "v0001",
    "Taxonomy ID": 56,
    "AVAILABLE_TOTAL_COUNT": 6859,
    "COMPLETENESS_COUNT": 741,
    "DISPERSION_RANGE1": 101,
    "DISPERSION_RANGE2": 392,
    "DISPERSION_RANGE3": 214,
    "DISPERSION_RANGE4": 32,
    "DISPERSION_RANGE5": 2
  },
  {
    "EFFECTIVE_DATE": "5/7/20",
    "Vendor": "v0001",
    "Taxonomy ID": 58,
    "AVAILABLE_TOTAL_COUNT": 6859,
    "COMPLETENESS_COUNT": 373,
    "DISPERSION_RANGE1": 19,
    "DISPERSION_RANGE2": 117,
    "DISPERSION_RANGE3": 187,
    "DISPERSION_RANGE4": 50,
    "DISPERSION_RANGE5": 0
  },
  {
    "EFFECTIVE_DATE": "5/7/20",
    "Vendor": "v0001",
    "Taxonomy ID": 49,
    "AVAILABLE_TOTAL_COUNT": 6859,
    "COMPLETENESS_COUNT": 661,
    "DISPERSION_RANGE1": 142,
    "DISPERSION_RANGE2": 255,
    "DISPERSION_RANGE3": 193,
    "DISPERSION_RANGE4": 65,
    "DISPERSION_RANGE5": 6
  },
  {
    "EFFECTIVE_DATE": "5/7/20",
    "Vendor": "v0001",
    "Taxonomy ID": 48,
    "AVAILABLE_TOTAL_COUNT": 6859,
    "COMPLETENESS_COUNT": 1571,
    "DISPERSION_RANGE1": 70,
    "DISPERSION_RANGE2": 242,
    "DISPERSION_RANGE3": 504,
    "DISPERSION_RANGE4": 518,
    "DISPERSION_RANGE5": 237
  },
  {
    "EFFECTIVE_DATE": "5/7/20",
    "Vendor": "v0001",
    "Taxonomy ID": 50,
    "AVAILABLE_TOTAL_COUNT": 6859,
    "COMPLETENESS_COUNT": 609,
    "DISPERSION_RANGE1": 14,
    "DISPERSION_RANGE2": 79,
    "DISPERSION_RANGE3": 219,
    "DISPERSION_RANGE4": 226,
    "DISPERSION_RANGE5": 71
  },
  {
    "EFFECTIVE_DATE": "5/7/20",
    "Vendor": "v0001",
    "Taxonomy ID": 53,
    "AVAILABLE_TOTAL_COUNT": 6859,
    "COMPLETENESS_COUNT": 133,
    "DISPERSION_RANGE1": 11,
    "DISPERSION_RANGE2": 33,
    "DISPERSION_RANGE3": 61,
    "DISPERSION_RANGE4": 21,
    "DISPERSION_RANGE5": 7
  },
  {
    "EFFECTIVE_DATE": "5/7/20",
    "Vendor": "v0001",
    "Taxonomy ID": 57,
    "AVAILABLE_TOTAL_COUNT": 6859,
    "COMPLETENESS_COUNT": 364,
    "DISPERSION_RANGE1": 63,
    "DISPERSION_RANGE2": 256,
    "DISPERSION_RANGE3": 35,
    "DISPERSION_RANGE4": 10,
    "DISPERSION_RANGE5": 0
  },
  {
    "EFFECTIVE_DATE": "5/7/20",
    "Vendor": "v0001",
    "Taxonomy ID": 67,
    "AVAILABLE_TOTAL_COUNT": 6859,
    "COMPLETENESS_COUNT": 6489,
    "DISPERSION_RANGE1": 4,
    "DISPERSION_RANGE2": 119,
    "DISPERSION_RANGE3": 1502,
    "DISPERSION_RANGE4": 3295,
    "DISPERSION_RANGE5": 1569
  },
  {
    "EFFECTIVE_DATE": "5/7/20",
    "Vendor": "v0001",
    "Taxonomy ID": 22,
    "AVAILABLE_TOTAL_COUNT": 20577,
    "COMPLETENESS_COUNT": 3633,
    "DISPERSION_RANGE1": 396,
    "DISPERSION_RANGE2": 939,
    "DISPERSION_RANGE3": 1225,
    "DISPERSION_RANGE4": 610,
    "DISPERSION_RANGE5": 463
  },
  {
    "EFFECTIVE_DATE": "5/7/20",
    "Vendor": "v0001",
    "Taxonomy ID": 21,
    "AVAILABLE_TOTAL_COUNT": 27436,
    "COMPLETENESS_COUNT": 6735,
    "DISPERSION_RANGE1": 590,
    "DISPERSION_RANGE2": 822,
    "DISPERSION_RANGE3": 1449,
    "DISPERSION_RANGE4": 2246,
    "DISPERSION_RANGE5": 1628
  },
  {
    "EFFECTIVE_DATE": "5/7/20",
    "Vendor": "v0001",
    "Taxonomy ID": 23,
    "AVAILABLE_TOTAL_COUNT": 20577,
    "COMPLETENESS_COUNT": 1981,
    "DISPERSION_RANGE1": 199,
    "DISPERSION_RANGE2": 605,
    "DISPERSION_RANGE3": 714,
    "DISPERSION_RANGE4": 397,
    "DISPERSION_RANGE5": 66
  },
  {
    "EFFECTIVE_DATE": "5/7/20",
    "Vendor": "v0001",
    "Taxonomy ID": 25,
    "AVAILABLE_TOTAL_COUNT": 27436,
    "COMPLETENESS_COUNT": 6522,
    "DISPERSION_RANGE1": 815,
    "DISPERSION_RANGE2": 2000,
    "DISPERSION_RANGE3": 2308,
    "DISPERSION_RANGE4": 1159,
    "DISPERSION_RANGE5": 240
  },
  {
    "EFFECTIVE_DATE": "5/7/20",
    "Vendor": "v0001",
    "Taxonomy ID": 28,
    "AVAILABLE_TOTAL_COUNT": 27436,
    "COMPLETENESS_COUNT": 1586,
    "DISPERSION_RANGE1": 192,
    "DISPERSION_RANGE2": 785,
    "DISPERSION_RANGE3": 461,
    "DISPERSION_RANGE4": 127,
    "DISPERSION_RANGE5": 21
  },
  {
    "EFFECTIVE_DATE": "5/7/20",
    "Vendor": "v0001",
    "Taxonomy ID": 27,
    "AVAILABLE_TOTAL_COUNT": 6859,
    "COMPLETENESS_COUNT": 346,
    "DISPERSION_RANGE1": 16,
    "DISPERSION_RANGE2": 101,
    "DISPERSION_RANGE3": 94,
    "DISPERSION_RANGE4": 95,
    "DISPERSION_RANGE5": 40
  },
  {
    "EFFECTIVE_DATE": "5/7/20",
    "Vendor": "v0001",
    "Taxonomy ID": 26,
    "AVAILABLE_TOTAL_COUNT": 41154,
    "COMPLETENESS_COUNT": 4090,
    "DISPERSION_RANGE1": 299,
    "DISPERSION_RANGE2": 1057,
    "DISPERSION_RANGE3": 1583,
    "DISPERSION_RANGE4": 895,
    "DISPERSION_RANGE5": 256
  },
  {
    "EFFECTIVE_DATE": "5/7/20",
    "Vendor": "v0001",
    "Taxonomy ID": 29,
    "AVAILABLE_TOTAL_COUNT": 27436,
    "COMPLETENESS_COUNT": 6839,
    "DISPERSION_RANGE1": 139,
    "DISPERSION_RANGE2": 937,
    "DISPERSION_RANGE3": 2746,
    "DISPERSION_RANGE4": 2620,
    "DISPERSION_RANGE5": 396
  },
  {
    "EFFECTIVE_DATE": "5/7/20",
    "Vendor": "v0001",
    "Taxonomy ID": 30,
    "AVAILABLE_TOTAL_COUNT": 34295,
    "COMPLETENESS_COUNT": 2935,
    "DISPERSION_RANGE1": 455,
    "DISPERSION_RANGE2": 1028,
    "DISPERSION_RANGE3": 907,
    "DISPERSION_RANGE4": 435,
    "DISPERSION_RANGE5": 110
  },
  {
    "EFFECTIVE_DATE": "5/7/20",
    "Vendor": "v0001",
    "Taxonomy ID": 18,
    "AVAILABLE_TOTAL_COUNT": 27436,
    "COMPLETENESS_COUNT": 6811,
    "DISPERSION_RANGE1": 654,
    "DISPERSION_RANGE2": 1996,
    "DISPERSION_RANGE3": 2103,
    "DISPERSION_RANGE4": 1497,
    "DISPERSION_RANGE5": 561
  },
  {
    "EFFECTIVE_DATE": "5/7/20",
    "Vendor": "v0001",
    "Taxonomy ID": 19,
    "AVAILABLE_TOTAL_COUNT": 27436,
    "COMPLETENESS_COUNT": 6822,
    "DISPERSION_RANGE1": 367,
    "DISPERSION_RANGE2": 2164,
    "DISPERSION_RANGE3": 3197,
    "DISPERSION_RANGE4": 975,
    "DISPERSION_RANGE5": 119
  },
  {
    "EFFECTIVE_DATE": "5/7/20",
    "Vendor": "v0001",
    "Taxonomy ID": 20,
    "AVAILABLE_TOTAL_COUNT": 13718,
    "COMPLETENESS_COUNT": 6848,
    "DISPERSION_RANGE1": 152,
    "DISPERSION_RANGE2": 1325,
    "DISPERSION_RANGE3": 3060,
    "DISPERSION_RANGE4": 2031,
    "DISPERSION_RANGE5": 280
  },
  {
    "EFFECTIVE_DATE": "5/7/20",
    "Vendor": "v0002",
    "Taxonomy ID": 108,
    "AVAILABLE_TOTAL_COUNT": 3376,
    "COMPLETENESS_COUNT": 1474,
    "DISPERSION_RANGE1": 278,
    "DISPERSION_RANGE2": 761,
    "DISPERSION_RANGE3": 359,
    "DISPERSION_RANGE4": 70,
    "DISPERSION_RANGE5": 6
  },
  {
    "EFFECTIVE_DATE": "5/7/20",
    "Vendor": "v0002",
    "Taxonomy ID": 110,
    "AVAILABLE_TOTAL_COUNT": 3376,
    "COMPLETENESS_COUNT": 1147,
    "DISPERSION_RANGE1": 241,
    "DISPERSION_RANGE2": 537,
    "DISPERSION_RANGE3": 314,
    "DISPERSION_RANGE4": 53,
    "DISPERSION_RANGE5": 2
  },
  {
    "EFFECTIVE_DATE": "5/7/20",
    "Vendor": "v0002",
    "Taxonomy ID": 81,
    "AVAILABLE_TOTAL_COUNT": 10128,
    "COMPLETENESS_COUNT": 2433,
    "DISPERSION_RANGE1": 753,
    "DISPERSION_RANGE2": 998,
    "DISPERSION_RANGE3": 547,
    "DISPERSION_RANGE4": 128,
    "DISPERSION_RANGE5": 7
  },
  {
    "EFFECTIVE_DATE": "5/7/20",
    "Vendor": "v0002",
    "Taxonomy ID": 111,
    "AVAILABLE_TOTAL_COUNT": 3376,
    "COMPLETENESS_COUNT": 576,
    "DISPERSION_RANGE1": 216,
    "DISPERSION_RANGE2": 307,
    "DISPERSION_RANGE3": 35,
    "DISPERSION_RANGE4": 18,
    "DISPERSION_RANGE5": 0
  },
  {
    "EFFECTIVE_DATE": "5/7/20",
    "Vendor": "v0002",
    "Taxonomy ID": 112,
    "AVAILABLE_TOTAL_COUNT": 3376,
    "COMPLETENESS_COUNT": 1836,
    "DISPERSION_RANGE1": 620,
    "DISPERSION_RANGE2": 800,
    "DISPERSION_RANGE3": 332,
    "DISPERSION_RANGE4": 75,
    "DISPERSION_RANGE5": 9
  },
  {
    "EFFECTIVE_DATE": "5/7/20",
    "Vendor": "v0002",
    "Taxonomy ID": 113,
    "AVAILABLE_TOTAL_COUNT": 3376,
    "COMPLETENESS_COUNT": 2210,
    "DISPERSION_RANGE1": 661,
    "DISPERSION_RANGE2": 745,
    "DISPERSION_RANGE3": 614,
    "DISPERSION_RANGE4": 170,
    "DISPERSION_RANGE5": 20
  },
  {
    "EFFECTIVE_DATE": "5/7/20",
    "Vendor": "v0002",
    "Taxonomy ID": 82,
    "AVAILABLE_TOTAL_COUNT": 10128,
    "COMPLETENESS_COUNT": 3376,
    "DISPERSION_RANGE1": 443,
    "DISPERSION_RANGE2": 2039,
    "DISPERSION_RANGE3": 835,
    "DISPERSION_RANGE4": 58,
    "DISPERSION_RANGE5": 1
  },
  {
    "EFFECTIVE_DATE": "5/7/20",
    "Vendor": "v0002",
    "Taxonomy ID": 114,
    "AVAILABLE_TOTAL_COUNT": 3376,
    "COMPLETENESS_COUNT": 3373,
    "DISPERSION_RANGE1": 219,
    "DISPERSION_RANGE2": 1445,
    "DISPERSION_RANGE3": 1443,
    "DISPERSION_RANGE4": 260,
    "DISPERSION_RANGE5": 6
  },
  {
    "EFFECTIVE_DATE": "5/7/20",
    "Vendor": "v0002",
    "Taxonomy ID": 115,
    "AVAILABLE_TOTAL_COUNT": 3376,
    "COMPLETENESS_COUNT": 2495,
    "DISPERSION_RANGE1": 571,
    "DISPERSION_RANGE2": 1181,
    "DISPERSION_RANGE3": 698,
    "DISPERSION_RANGE4": 44,
    "DISPERSION_RANGE5": 1
  },
  {
    "EFFECTIVE_DATE": "5/7/20",
    "Vendor": "v0002",
    "Taxonomy ID": 116,
    "AVAILABLE_TOTAL_COUNT": 3376,
    "COMPLETENESS_COUNT": 3008,
    "DISPERSION_RANGE1": 1859,
    "DISPERSION_RANGE2": 851,
    "DISPERSION_RANGE3": 275,
    "DISPERSION_RANGE4": 23,
    "DISPERSION_RANGE5": 0
  },
  {
    "EFFECTIVE_DATE": "5/7/20",
    "Vendor": "v0002",
    "Taxonomy ID": 85,
    "AVAILABLE_TOTAL_COUNT": 3376,
    "COMPLETENESS_COUNT": 3351,
    "DISPERSION_RANGE1": 203,
    "DISPERSION_RANGE2": 1029,
    "DISPERSION_RANGE3": 1302,
    "DISPERSION_RANGE4": 724,
    "DISPERSION_RANGE5": 93
  },
  {
    "EFFECTIVE_DATE": "5/7/20",
    "Vendor": "v0002",
    "Taxonomy ID": 120,
    "AVAILABLE_TOTAL_COUNT": 3376,
    "COMPLETENESS_COUNT": 3351,
    "DISPERSION_RANGE1": 203,
    "DISPERSION_RANGE2": 1029,
    "DISPERSION_RANGE3": 1302,
    "DISPERSION_RANGE4": 724,
    "DISPERSION_RANGE5": 93
  },
  {
    "EFFECTIVE_DATE": "5/7/20",
    "Vendor": "v0002",
    "Taxonomy ID": 86,
    "AVAILABLE_TOTAL_COUNT": 3376,
    "COMPLETENESS_COUNT": 3372,
    "DISPERSION_RANGE1": 191,
    "DISPERSION_RANGE2": 721,
    "DISPERSION_RANGE3": 1597,
    "DISPERSION_RANGE4": 833,
    "DISPERSION_RANGE5": 30
  },
  {
    "EFFECTIVE_DATE": "5/7/20",
    "Vendor": "v0002",
    "Taxonomy ID": 121,
    "AVAILABLE_TOTAL_COUNT": 3376,
    "COMPLETENESS_COUNT": 3372,
    "DISPERSION_RANGE1": 191,
    "DISPERSION_RANGE2": 721,
    "DISPERSION_RANGE3": 1597,
    "DISPERSION_RANGE4": 833,
    "DISPERSION_RANGE5": 30
  },
  {
    "EFFECTIVE_DATE": "5/7/20",
    "Vendor": "v0002",
    "Taxonomy ID": 87,
    "AVAILABLE_TOTAL_COUNT": 3376,
    "COMPLETENESS_COUNT": 3363,
    "DISPERSION_RANGE1": 144,
    "DISPERSION_RANGE2": 594,
    "DISPERSION_RANGE3": 1775,
    "DISPERSION_RANGE4": 788,
    "DISPERSION_RANGE5": 62
  },
  {
    "EFFECTIVE_DATE": "5/7/20",
    "Vendor": "v0002",
    "Taxonomy ID": 122,
    "AVAILABLE_TOTAL_COUNT": 3376,
    "COMPLETENESS_COUNT": 3363,
    "DISPERSION_RANGE1": 144,
    "DISPERSION_RANGE2": 594,
    "DISPERSION_RANGE3": 1775,
    "DISPERSION_RANGE4": 788,
    "DISPERSION_RANGE5": 62
  },
  {
    "EFFECTIVE_DATE": "5/7/20",
    "Vendor": "v0002",
    "Taxonomy ID": 88,
    "AVAILABLE_TOTAL_COUNT": 3376,
    "COMPLETENESS_COUNT": 3374,
    "DISPERSION_RANGE1": 842,
    "DISPERSION_RANGE2": 1247,
    "DISPERSION_RANGE3": 1013,
    "DISPERSION_RANGE4": 258,
    "DISPERSION_RANGE5": 14
  },
  {
    "EFFECTIVE_DATE": "5/7/20",
    "Vendor": "v0002",
    "Taxonomy ID": 123,
    "AVAILABLE_TOTAL_COUNT": 3376,
    "COMPLETENESS_COUNT": 3374,
    "DISPERSION_RANGE1": 842,
    "DISPERSION_RANGE2": 1247,
    "DISPERSION_RANGE3": 1013,
    "DISPERSION_RANGE4": 258,
    "DISPERSION_RANGE5": 14
  },
  {
    "EFFECTIVE_DATE": "5/7/20",
    "Vendor": "v0002",
    "Taxonomy ID": 83,
    "AVAILABLE_TOTAL_COUNT": 6752,
    "COMPLETENESS_COUNT": 3322,
    "DISPERSION_RANGE1": 949,
    "DISPERSION_RANGE2": 1660,
    "DISPERSION_RANGE3": 559,
    "DISPERSION_RANGE4": 148,
    "DISPERSION_RANGE5": 6
  },
  {
    "EFFECTIVE_DATE": "5/7/20",
    "Vendor": "v0002",
    "Taxonomy ID": 117,
    "AVAILABLE_TOTAL_COUNT": 3376,
    "COMPLETENESS_COUNT": 2699,
    "DISPERSION_RANGE1": 732,
    "DISPERSION_RANGE2": 1292,
    "DISPERSION_RANGE3": 547,
    "DISPERSION_RANGE4": 125,
    "DISPERSION_RANGE5": 3
  },
  {
    "EFFECTIVE_DATE": "5/7/20",
    "Vendor": "v0002",
    "Taxonomy ID": 84,
    "AVAILABLE_TOTAL_COUNT": 6752,
    "COMPLETENESS_COUNT": 2207,
    "DISPERSION_RANGE1": 778,
    "DISPERSION_RANGE2": 967,
    "DISPERSION_RANGE3": 342,
    "DISPERSION_RANGE4": 105,
    "DISPERSION_RANGE5": 15
  },
  {
    "EFFECTIVE_DATE": "5/7/20",
    "Vendor": "v0002",
    "Taxonomy ID": 118,
    "AVAILABLE_TOTAL_COUNT": 3376,
    "COMPLETENESS_COUNT": 1617,
    "DISPERSION_RANGE1": 690,
    "DISPERSION_RANGE2": 636,
    "DISPERSION_RANGE3": 221,
    "DISPERSION_RANGE4": 62,
    "DISPERSION_RANGE5": 8
  },
  {
    "EFFECTIVE_DATE": "5/7/20",
    "Vendor": "v0002",
    "Taxonomy ID": 119,
    "AVAILABLE_TOTAL_COUNT": 3376,
    "COMPLETENESS_COUNT": 979,
    "DISPERSION_RANGE1": 213,
    "DISPERSION_RANGE2": 468,
    "DISPERSION_RANGE3": 208,
    "DISPERSION_RANGE4": 79,
    "DISPERSION_RANGE5": 11
  },
  {
    "EFFECTIVE_DATE": "5/7/20",
    "Vendor": "v0002",
    "Taxonomy ID": 77,
    "AVAILABLE_TOTAL_COUNT": 13504,
    "COMPLETENESS_COUNT": 3240,
    "DISPERSION_RANGE1": 727,
    "DISPERSION_RANGE2": 1184,
    "DISPERSION_RANGE3": 819,
    "DISPERSION_RANGE4": 406,
    "DISPERSION_RANGE5": 104
  },
  {
    "EFFECTIVE_DATE": "5/7/20",
    "Vendor": "v0002",
    "Taxonomy ID": 98,
    "AVAILABLE_TOTAL_COUNT": 3376,
    "COMPLETENESS_COUNT": 1082,
    "DISPERSION_RANGE1": 288,
    "DISPERSION_RANGE2": 436,
    "DISPERSION_RANGE3": 273,
    "DISPERSION_RANGE4": 77,
    "DISPERSION_RANGE5": 8
  },
  {
    "EFFECTIVE_DATE": "5/7/20",
    "Vendor": "v0002",
    "Taxonomy ID": 99,
    "AVAILABLE_TOTAL_COUNT": 3376,
    "COMPLETENESS_COUNT": 688,
    "DISPERSION_RANGE1": 212,
    "DISPERSION_RANGE2": 264,
    "DISPERSION_RANGE3": 127,
    "DISPERSION_RANGE4": 56,
    "DISPERSION_RANGE5": 29
  },
  {
    "EFFECTIVE_DATE": "5/7/20",
    "Vendor": "v0002",
    "Taxonomy ID": 78,
    "AVAILABLE_TOTAL_COUNT": 20256,
    "COMPLETENESS_COUNT": 3079,
    "DISPERSION_RANGE1": 1195,
    "DISPERSION_RANGE2": 1198,
    "DISPERSION_RANGE3": 559,
    "DISPERSION_RANGE4": 117,
    "DISPERSION_RANGE5": 10
  },
  {
    "EFFECTIVE_DATE": "5/7/20",
    "Vendor": "v0002",
    "Taxonomy ID": 101,
    "AVAILABLE_TOTAL_COUNT": 3376,
    "COMPLETENESS_COUNT": 1161,
    "DISPERSION_RANGE1": 389,
    "DISPERSION_RANGE2": 529,
    "DISPERSION_RANGE3": 213,
    "DISPERSION_RANGE4": 29,
    "DISPERSION_RANGE5": 1
  },
  {
    "EFFECTIVE_DATE": "5/7/20",
    "Vendor": "v0002",
    "Taxonomy ID": 102,
    "AVAILABLE_TOTAL_COUNT": 3376,
    "COMPLETENESS_COUNT": 2907,
    "DISPERSION_RANGE1": 767,
    "DISPERSION_RANGE2": 1024,
    "DISPERSION_RANGE3": 799,
    "DISPERSION_RANGE4": 275,
    "DISPERSION_RANGE5": 42
  },
  {
    "EFFECTIVE_DATE": "5/7/20",
    "Vendor": "v0002",
    "Taxonomy ID": 103,
    "AVAILABLE_TOTAL_COUNT": 3376,
    "COMPLETENESS_COUNT": 771,
    "DISPERSION_RANGE1": 303,
    "DISPERSION_RANGE2": 313,
    "DISPERSION_RANGE3": 125,
    "DISPERSION_RANGE4": 27,
    "DISPERSION_RANGE5": 3
  },
  {
    "EFFECTIVE_DATE": "5/7/20",
    "Vendor": "v0002",
    "Taxonomy ID": 104,
    "AVAILABLE_TOTAL_COUNT": 3376,
    "COMPLETENESS_COUNT": 1192,
    "DISPERSION_RANGE1": 423,
    "DISPERSION_RANGE2": 521,
    "DISPERSION_RANGE3": 213,
    "DISPERSION_RANGE4": 33,
    "DISPERSION_RANGE5": 2
  },
  {
    "EFFECTIVE_DATE": "5/7/20",
    "Vendor": "v0002",
    "Taxonomy ID": 106,
    "AVAILABLE_TOTAL_COUNT": 3376,
    "COMPLETENESS_COUNT": 1027,
    "DISPERSION_RANGE1": 455,
    "DISPERSION_RANGE2": 361,
    "DISPERSION_RANGE3": 172,
    "DISPERSION_RANGE4": 37,
    "DISPERSION_RANGE5": 2
  },
  {
    "EFFECTIVE_DATE": "5/7/20",
    "Vendor": "v0002",
    "Taxonomy ID": 107,
    "AVAILABLE_TOTAL_COUNT": 3376,
    "COMPLETENESS_COUNT": 1248,
    "DISPERSION_RANGE1": 605,
    "DISPERSION_RANGE2": 361,
    "DISPERSION_RANGE3": 192,
    "DISPERSION_RANGE4": 77,
    "DISPERSION_RANGE5": 13
  },
  {
    "EFFECTIVE_DATE": "5/7/20",
    "Vendor": "v0002",
    "Taxonomy ID": 97,
    "AVAILABLE_TOTAL_COUNT": 3376,
    "COMPLETENESS_COUNT": 1248,
    "DISPERSION_RANGE1": 605,
    "DISPERSION_RANGE2": 361,
    "DISPERSION_RANGE3": 192,
    "DISPERSION_RANGE4": 77,
    "DISPERSION_RANGE5": 13
  },
  {
    "EFFECTIVE_DATE": "5/7/20",
    "Vendor": "v0002",
    "Taxonomy ID": 74,
    "AVAILABLE_TOTAL_COUNT": 3376,
    "COMPLETENESS_COUNT": 3367,
    "DISPERSION_RANGE1": 2075,
    "DISPERSION_RANGE2": 699,
    "DISPERSION_RANGE3": 379,
    "DISPERSION_RANGE4": 179,
    "DISPERSION_RANGE5": 35
  },
  {
    "EFFECTIVE_DATE": "5/7/20",
    "Vendor": "v0002",
    "Taxonomy ID": 91,
    "AVAILABLE_TOTAL_COUNT": 3376,
    "COMPLETENESS_COUNT": 3367,
    "DISPERSION_RANGE1": 2075,
    "DISPERSION_RANGE2": 699,
    "DISPERSION_RANGE3": 379,
    "DISPERSION_RANGE4": 179,
    "DISPERSION_RANGE5": 35
  },
  {
    "EFFECTIVE_DATE": "5/7/20",
    "Vendor": "v0002",
    "Taxonomy ID": 75,
    "AVAILABLE_TOTAL_COUNT": 6752,
    "COMPLETENESS_COUNT": 3373,
    "DISPERSION_RANGE1": 1826,
    "DISPERSION_RANGE2": 1250,
    "DISPERSION_RANGE3": 224,
    "DISPERSION_RANGE4": 73,
    "DISPERSION_RANGE5": 0
  },
  {
    "EFFECTIVE_DATE": "5/7/20",
    "Vendor": "v0002",
    "Taxonomy ID": 92,
    "AVAILABLE_TOTAL_COUNT": 3376,
    "COMPLETENESS_COUNT": 3352,
    "DISPERSION_RANGE1": 2307,
    "DISPERSION_RANGE2": 815,
    "DISPERSION_RANGE3": 183,
    "DISPERSION_RANGE4": 44,
    "DISPERSION_RANGE5": 3
  },
  {
    "EFFECTIVE_DATE": "5/7/20",
    "Vendor": "v0002",
    "Taxonomy ID": 93,
    "AVAILABLE_TOTAL_COUNT": 3376,
    "COMPLETENESS_COUNT": 3183,
    "DISPERSION_RANGE1": 975,
    "DISPERSION_RANGE2": 1532,
    "DISPERSION_RANGE3": 506,
    "DISPERSION_RANGE4": 155,
    "DISPERSION_RANGE5": 15
  },
  {
    "EFFECTIVE_DATE": "5/7/20",
    "Vendor": "v0002",
    "Taxonomy ID": 76,
    "AVAILABLE_TOTAL_COUNT": 10128,
    "COMPLETENESS_COUNT": 3369,
    "DISPERSION_RANGE1": 1037,
    "DISPERSION_RANGE2": 1358,
    "DISPERSION_RANGE3": 773,
    "DISPERSION_RANGE4": 189,
    "DISPERSION_RANGE5": 12
  },
  {
    "EFFECTIVE_DATE": "5/7/20",
    "Vendor": "v0002",
    "Taxonomy ID": 94,
    "AVAILABLE_TOTAL_COUNT": 3376,
    "COMPLETENESS_COUNT": 402,
    "DISPERSION_RANGE1": 299,
    "DISPERSION_RANGE2": 82,
    "DISPERSION_RANGE3": 15,
    "DISPERSION_RANGE4": 6,
    "DISPERSION_RANGE5": 0
  },
  {
    "EFFECTIVE_DATE": "5/7/20",
    "Vendor": "v0002",
    "Taxonomy ID": 95,
    "AVAILABLE_TOTAL_COUNT": 3376,
    "COMPLETENESS_COUNT": 3324,
    "DISPERSION_RANGE1": 845,
    "DISPERSION_RANGE2": 1445,
    "DISPERSION_RANGE3": 819,
    "DISPERSION_RANGE4": 203,
    "DISPERSION_RANGE5": 12
  },
  {
    "EFFECTIVE_DATE": "5/7/20",
    "Vendor": "v0002",
    "Taxonomy ID": 96,
    "AVAILABLE_TOTAL_COUNT": 3376,
    "COMPLETENESS_COUNT": 692,
    "DISPERSION_RANGE1": 520,
    "DISPERSION_RANGE2": 142,
    "DISPERSION_RANGE3": 23,
    "DISPERSION_RANGE4": 7,
    "DISPERSION_RANGE5": 0
  },
  {
    "EFFECTIVE_DATE": "5/7/20",
    "Vendor": "v0002",
    "Taxonomy ID": 89,
    "AVAILABLE_TOTAL_COUNT": 6752,
    "COMPLETENESS_COUNT": 3376,
    "DISPERSION_RANGE1": 233,
    "DISPERSION_RANGE2": 2037,
    "DISPERSION_RANGE3": 933,
    "DISPERSION_RANGE4": 168,
    "DISPERSION_RANGE5": 5
  },
  {
    "EFFECTIVE_DATE": "5/7/20",
    "Vendor": "v0002",
    "Taxonomy ID": 124,
    "AVAILABLE_TOTAL_COUNT": 3376,
    "COMPLETENESS_COUNT": 2432,
    "DISPERSION_RANGE1": 75,
    "DISPERSION_RANGE2": 1265,
    "DISPERSION_RANGE3": 832,
    "DISPERSION_RANGE4": 231,
    "DISPERSION_RANGE5": 29
  },
  {
    "EFFECTIVE_DATE": "5/7/20",
    "Vendor": "v0002",
    "Taxonomy ID": 90,
    "AVAILABLE_TOTAL_COUNT": 10128,
    "COMPLETENESS_COUNT": 3376,
    "DISPERSION_RANGE1": 305,
    "DISPERSION_RANGE2": 2079,
    "DISPERSION_RANGE3": 821,
    "DISPERSION_RANGE4": 161,
    "DISPERSION_RANGE5": 10
  },
  {
    "EFFECTIVE_DATE": "5/7/20",
    "Vendor": "v0002",
    "Taxonomy ID": 125,
    "AVAILABLE_TOTAL_COUNT": 3376,
    "COMPLETENESS_COUNT": 3362,
    "DISPERSION_RANGE1": 35,
    "DISPERSION_RANGE2": 2887,
    "DISPERSION_RANGE3": 336,
    "DISPERSION_RANGE4": 91,
    "DISPERSION_RANGE5": 13
  },
  {
    "EFFECTIVE_DATE": "5/7/20",
    "Vendor": "v0002",
    "Taxonomy ID": 126,
    "AVAILABLE_TOTAL_COUNT": 3376,
    "COMPLETENESS_COUNT": 3376,
    "DISPERSION_RANGE1": 391,
    "DISPERSION_RANGE2": 1295,
    "DISPERSION_RANGE3": 1223,
    "DISPERSION_RANGE4": 428,
    "DISPERSION_RANGE5": 39
  },
  {
    "EFFECTIVE_DATE": "5/7/20",
    "Vendor": "v0002",
    "Taxonomy ID": 127,
    "AVAILABLE_TOTAL_COUNT": 3376,
    "COMPLETENESS_COUNT": 12,
    "DISPERSION_RANGE1": 2,
    "DISPERSION_RANGE2": 9,
    "DISPERSION_RANGE3": 1,
    "DISPERSION_RANGE4": 0,
    "DISPERSION_RANGE5": 0
  },
  {
    "EFFECTIVE_DATE": "5/7/20",
    "Vendor": "v0002",
    "Taxonomy ID": 68,
    "AVAILABLE_TOTAL_COUNT": 3376,
    "COMPLETENESS_COUNT": 3369,
    "DISPERSION_RANGE1": 1310,
    "DISPERSION_RANGE2": 1766,
    "DISPERSION_RANGE3": 257,
    "DISPERSION_RANGE4": 36,
    "DISPERSION_RANGE5": 0
  },
  {
    "EFFECTIVE_DATE": "5/7/20",
    "Vendor": "v0002",
    "Taxonomy ID": 69,
    "AVAILABLE_TOTAL_COUNT": 3376,
    "COMPLETENESS_COUNT": 3364,
    "DISPERSION_RANGE1": 1144,
    "DISPERSION_RANGE2": 1869,
    "DISPERSION_RANGE3": 287,
    "DISPERSION_RANGE4": 64,
    "DISPERSION_RANGE5": 0
  },
  {
    "EFFECTIVE_DATE": "5/7/20",
    "Vendor": "v0002",
    "Taxonomy ID": 70,
    "AVAILABLE_TOTAL_COUNT": 3376,
    "COMPLETENESS_COUNT": 3369,
    "DISPERSION_RANGE1": 216,
    "DISPERSION_RANGE2": 2821,
    "DISPERSION_RANGE3": 296,
    "DISPERSION_RANGE4": 36,
    "DISPERSION_RANGE5": 0
  },
  {
    "EFFECTIVE_DATE": "5/7/20",
    "Vendor": "v0002",
    "Taxonomy ID": 71,
    "AVAILABLE_TOTAL_COUNT": 3376,
    "COMPLETENESS_COUNT": 3367,
    "DISPERSION_RANGE1": 683,
    "DISPERSION_RANGE2": 2414,
    "DISPERSION_RANGE3": 220,
    "DISPERSION_RANGE4": 50,
    "DISPERSION_RANGE5": 0
  },
  {
    "EFFECTIVE_DATE": "5/7/20",
    "Vendor": "v0002",
    "Taxonomy ID": 72,
    "AVAILABLE_TOTAL_COUNT": 3376,
    "COMPLETENESS_COUNT": 3369,
    "DISPERSION_RANGE1": 9,
    "DISPERSION_RANGE2": 1310,
    "DISPERSION_RANGE3": 1988,
    "DISPERSION_RANGE4": 62,
    "DISPERSION_RANGE5": 0
  },
  {
    "EFFECTIVE_DATE": "5/7/20",
    "Vendor": "v0002",
    "Taxonomy ID": 73,
    "AVAILABLE_TOTAL_COUNT": 3376,
    "COMPLETENESS_COUNT": 3369,
    "DISPERSION_RANGE1": 45,
    "DISPERSION_RANGE2": 2751,
    "DISPERSION_RANGE3": 494,
    "DISPERSION_RANGE4": 79,
    "DISPERSION_RANGE5": 0
  },
  {
    "EFFECTIVE_DATE": "5/7/20",
    "Vendor": "v0002",
    "Taxonomy ID": 100,
    "AVAILABLE_TOTAL_COUNT": 3376,
    "COMPLETENESS_COUNT": 402,
    "DISPERSION_RANGE1": 299,
    "DISPERSION_RANGE2": 82,
    "DISPERSION_RANGE3": 15,
    "DISPERSION_RANGE4": 6,
    "DISPERSION_RANGE5": 0
  },
  {
    "EFFECTIVE_DATE": "5/7/20",
    "Vendor": "v0002",
    "Taxonomy ID": 105,
    "AVAILABLE_TOTAL_COUNT": 3376,
    "COMPLETENESS_COUNT": 3324,
    "DISPERSION_RANGE1": 845,
    "DISPERSION_RANGE2": 1445,
    "DISPERSION_RANGE3": 819,
    "DISPERSION_RANGE4": 203,
    "DISPERSION_RANGE5": 12
  },
  {
    "EFFECTIVE_DATE": "5/7/20",
    "Vendor": "v0002",
    "Taxonomy ID": 109,
    "AVAILABLE_TOTAL_COUNT": 3376,
    "COMPLETENESS_COUNT": 692,
    "DISPERSION_RANGE1": 520,
    "DISPERSION_RANGE2": 142,
    "DISPERSION_RANGE3": 23,
    "DISPERSION_RANGE4": 7,
    "DISPERSION_RANGE5": 0
  },
  {
    "EFFECTIVE_DATE": "5/7/20",
    "Vendor": "v0002",
    "Taxonomy ID": 79,
    "AVAILABLE_TOTAL_COUNT": 10128,
    "COMPLETENESS_COUNT": 3376,
    "DISPERSION_RANGE1": 305,
    "DISPERSION_RANGE2": 2079,
    "DISPERSION_RANGE3": 821,
    "DISPERSION_RANGE4": 161,
    "DISPERSION_RANGE5": 10
  },
  {
    "EFFECTIVE_DATE": "5/7/20",
    "Vendor": "v0002",
    "Taxonomy ID": 80,
    "AVAILABLE_TOTAL_COUNT": 10128,
    "COMPLETENESS_COUNT": 3376,
    "DISPERSION_RANGE1": 305,
    "DISPERSION_RANGE2": 2079,
    "DISPERSION_RANGE3": 821,
    "DISPERSION_RANGE4": 161,
    "DISPERSION_RANGE5": 10
  }
]
