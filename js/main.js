import store from './store/index.js'

import Grid from './components/grid.js'
import ColChart from './components/col-chart.js'
import PieChart from './components/pie-chart.js'
import RadarChart from './components/radar-chart.js'

const ws = new WebSocket('ws://localhost:8080')

ws.onopen = (event) => {
  console.log('Connected to the WebSocket server on ws://localhost:8080!')

  // Instantiate components
  const grid = new Grid(ws)
  const colChart = new ColChart(ws)
  const pieChart = new PieChart(ws)
  // const radarChart = new RadarChart(ws)

  // Initial renders
  grid.render()
  colChart.render()
  pieChart.render()
  radarChart.render()

  // subscribe to updates for the first item
  ws.send(`subscribe/${store.state.selectedItem}`)
}



