const WebSocket = require('ws')

const ws = new WebSocket('ws://localhost:8080')

ws.on('open', () => {
  console.log("Connected!")
})

ws.on('message', (data) => {
  console.log(JSON.parse(data))
})
