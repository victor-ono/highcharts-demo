export default {
    setSelectedItem(state, payload) {
        state.selectedItem = payload
        return state
    }
}
